# zupan-tts

## ✅ Development Environment Requirements

- [Flutter SDK](https://flutter.dev/docs/get-started/install)
- [Android Studio](https://developer.android.com/studio) is recommended for the main development IDE and the following plugins are required
    - https://plugins.jetbrains.com/plugin/9212-flutter/
    - https://plugins.jetbrains.com/plugin/6351-dart/
- [XCode](https://developer.apple.com/xcode)

## :computer: Git

**How to enable pre-commit githook**

```bash
git config core.hooksPath .githooks/
cd .githooks
chmod +x pre-commit.sh
```

Commit messages are written the following way [Semantic Commit Messages Guide](https://gist.github.com/joshbuchea/6f47e86d2510bce28f8e7f42ae84c716)

**Workflow**

1. **git checkout development**
2. **git pull origin development** (the development branch always needs to be up-to-date)
3. **git checkout -b feature/DEV + issue/some-descriptive-name**
4. Do some coding magic...
5. **git push -u origin feature/DEV + issue/some-descriptive-name**
6. Repeat step 1. & 2. and create a new feature branch